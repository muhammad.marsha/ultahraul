from django.apps import AppConfig


class WebkuConfig(AppConfig):
    name = 'webku'
